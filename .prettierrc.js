module.exports = {
	printWidth: 100,
	trailingComma: 'es5',
	semi: false,
	singleQuote: true,
	jsxBracketSameLine: false,
	bracketSpacing: true,
	arrowParens: 'avoid',
	endOfLine: 'lf',
	useTabs: true,
	tabWidth: 4,
}
