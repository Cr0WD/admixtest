import React from 'react'
import GlobalStyle from 'src/styles/global'
import { SearchBar } from './components/SearchBar'
import styled, { ThemeProvider } from 'styled-components'
import theme from 'src/styles/theme'
import AppsList from 'src/components/AppsList'
import { RecoilRoot } from 'recoil'
import DropdownMenu from 'src/components/DropdownMenu'
import Modal from 'src/components/modal/Modal'

const containerInner = 1288
const containerPadding = 16

const Container = styled.div`
	width: 100%;
	max-width: ${containerInner + containerPadding * 2}px;
	margin: auto;
	padding: 40px ${containerPadding}px;
	display: grid;
`

const App = () => {
	return (
		<>
			<ThemeProvider theme={theme}>
				<RecoilRoot>
					<Container>
						<SearchBar />
						<DropdownMenu />
						<AppsList />
					</Container>
					<Modal />
				</RecoilRoot>
				<GlobalStyle />
			</ThemeProvider>
		</>
	)
}

export default App
