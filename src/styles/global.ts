import { createGlobalStyle } from 'styled-components'
import { normalize } from 'polished'

export const GlobalStyle = createGlobalStyle`
	${normalize()}
	* {
		font-family: 'Mulish', sans-serif;
		box-sizing: border-box;
	}


	html {
		height: 100%;
	}

	body {
		background: ${({ theme }) => theme.background};
	}
`

export default GlobalStyle
