const theme = {
	background: '#f8f9fb',
	white: '#fff',
	black: '#232747',
	backdrop: '#c4Cad6',
	black600: '#6D6F84', //Figma naming
	primary: '#2622b5',
	secondary: '#0c0980',
	gray: '#dcdfee',
	grayText: '#9d9fad',

	searchBar: {
		radius: '10px',
	},

	card: {
		iconsBar: {
			background: '#0b0f32',
		},
	},
}

/*
 * Extend style-components with theme type
 * */
type ITheme = typeof theme
declare module 'styled-components' {
	export interface DefaultTheme extends ITheme {}
}

export default theme
