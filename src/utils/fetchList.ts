import urls from 'src/constants/urls'
import { Body } from 'src/interfaces/request'
import { App } from 'src/interfaces/app'

interface Result {
	items: App[]
	totalCount: number
	dau?: number
}
export const fetchList = async (params?: Body) => {
	let result: Result = {
		items: [],
		totalCount: 0,
	}
	try {
		const response = await fetch(urls.api, {
			method: 'POST',
			body: JSON.stringify(params ?? {}),
			headers: new Headers({
				'admix-api-key': process.env.REACT_APP_API_KEY ?? '',
			}),
		})
		const responseJson = await response.json()
		const { items, totalCount } = responseJson.data
		result['items'] = items
		result['totalCount'] = totalCount
	} catch (e) {
		console.error(e)
	}
	return result
}
