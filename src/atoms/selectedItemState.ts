import { atom } from 'recoil'
import { App } from 'src/interfaces/app'

export const selectedItemState = atom<App | {}>({
	key: 'selectedItemState',
	default: {},
})
