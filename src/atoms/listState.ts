import { atom } from 'recoil'
import { App } from 'src/interfaces/app'

export const listState = atom<App[]>({
	key: 'listState',
	default: [],
})
