import { atom } from 'recoil'
import labels from 'src/constants/labels'

export const categoryState = atom<string>({
	key: 'categoryState',
	default: labels.defaultCategory,
})
