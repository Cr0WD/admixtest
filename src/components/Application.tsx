import { App } from 'src/interfaces/app'
import { FC, memo } from 'react'
import styled from 'styled-components'
import { Skeleton } from 'antd'
import nFormatter from 'src/utils/nFormatter'
import labels from 'src/constants/labels'

import MedalIcon from '../images/svg/medal.svg'
import ChildrenIcon from '../images/svg/children.svg'
import settings from 'src/constants/settings'
import { useSetRecoilState } from 'recoil'
import { selectedItemState } from 'src/atoms/selectedItemState'
import { modalVisibleState } from 'src/atoms/modalVisibleState'

const Application: FC<{
	item?: App
}> = ({ item }) => {
	const setSelectedItem = useSetRecoilState(selectedItemState)
	const setModalVisibleState = useSetRecoilState(modalVisibleState)

	const { availsTitle, childrenAlt, medalAlt } = labels.card
	const { title, avails, googlePlayStoreInfo, appStoreInfo } = item ?? {}
	const { screenshots, contentRating, score } = googlePlayStoreInfo ?? appStoreInfo ?? {}

	/*
	 * Use screenshot placeholder
	 * */
	const image =
		(screenshots && screenshots[0]) ??
		`https://dummyimage.com/304X180/ffffff/000.png&text=${encodeURI(
			title?.toUpperCase() ?? ''
		)}`

	const withMedal = score && score > 4
	const childRating = contentRating && new Set(settings.childRating).has(contentRating)

	const iconsBarShown = withMedal || childRating
	return (
		<>
			{title ? (
				<StyledCard
					onClick={() => {
						if (!item) return
						setSelectedItem(item)
						setModalVisibleState(true)
					}}
				>
					<div className="imageContainer" style={{ backgroundImage: `url(${image})` }}>
						{iconsBarShown && (
							<div className="iconsBar">
								{withMedal && <img src={MedalIcon} alt={medalAlt} />}
								{childRating && <img src={ChildrenIcon} alt={childrenAlt} />}
							</div>
						)}
					</div>

					<div className="textContainer">
						<div className="description">
							<h3 className="title">{title}</h3>
							<div className="rating">{contentRating}</div>
						</div>

						<div className="avails">
							<div className="count">{nFormatter(avails ?? 0, 1)}</div>
							<div className="title">{availsTitle}</div>
						</div>
					</div>
				</StyledCard>
			) : (
				<Skeleton.Input active style={{ height: 235, borderRadius: 8 }} />
			)}
		</>
	)
}
export default memo(Application)

const StyledCard = styled.div`
	.textContainer {
		padding-top: 16px;
		display: grid;
		grid-template-columns: auto auto;
		position: relative;
		gap: 16px;

		.description {
			display: grid;
			gap: 3px;

			.title {
				overflow: hidden;
				max-width: 100%;
				white-space: nowrap;
				text-overflow: ellipsis;
				font-weight: bold;
				font-size: 16px;
				line-height: 112%;
				color: ${({ theme }) => theme.black};
				margin: 0;
				padding: 0;
			}

			.rating {
				font-weight: 600;
				font-size: 13px;
				line-height: 138%;

				color: ${({ theme }) => theme.grayText};
			}
		}

		.avails {
			display: grid;
			gap: 3px;
			text-align: right;

			.count {
				font-size: 16px;
				line-height: 112%;

				font-weight: 800;
				color: ${({ theme }) => theme.black};
			}

			.title {
				font-weight: 600;
				font-size: 13px;
				line-height: 138%;

				color: ${({ theme }) => theme.grayText};
			}
		}
	}

	.imageContainer {
		user-select: none;
		width: 304px;
		height: 180px;
		background-position: center;
		background-size: cover;
		position: relative;
		display: grid;
		align-items: end;
		border-radius: 8px;

		.iconsBar {
			&:before {
				content: '';
				display: block;
				position: absolute;
				top: 0;
				left: 0;
				width: 100%;
				height: 100%;
				opacity: 0.9;
				border-bottom-left-radius: 8px;
				border-bottom-right-radius: 8px;
				background: linear-gradient(
					90deg,
					transparent 0%,
					${({ theme }) => theme.card.iconsBar.background} 100%
				);
			}

			position: relative;

			padding: 8px 12px;
			display: grid;
			gap: 8px;
			grid-auto-flow: column;
			justify-content: end;

			img,
			svg {
				position: relative;
				display: block;
				width: 24px;
				height: 24px;
			}
		}
	}

	cursor: pointer;

	transition: transform 0.15s ease-in-out;

	&:hover {
		transform: scale(1.05);
	}

	&:active {
		transform: scale(1.03);
	}
`
