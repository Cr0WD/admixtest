import { memo } from 'react'
import { useRecoilState } from 'recoil'
import { Modal as ANTDModal } from 'antd'
import { modalVisibleState } from 'src/atoms/modalVisibleState'
import { ReactComponent as CloseIcon } from 'src/images/svg/close.svg'
import styled, { useTheme } from 'styled-components'
import RightSide from 'src/components/modal/RightSide'
import LeftSide from 'src/components/modal/LeftSide'
import { rgba } from 'polished'

const Modal = () => {
	const [visible, setVisible] = useRecoilState(modalVisibleState)
	const theme = useTheme()
	return (
		<>
			<StyledModal
				maskStyle={{
					backgroundColor: rgba(theme.backdrop, 0.8),
				}}
				destroyOnClose
				closeIcon={<CloseIcon />}
				visible={visible}
				onCancel={() => setVisible(false)}
				footer={null}
			>
				<LeftSide />
				<RightSide />
			</StyledModal>
		</>
	)
}

const StyledModal = styled(ANTDModal)`
	width: 1444px !important;
	top: 0;
	padding: 30px 0;
	display: flex;
	flex-direction: column;
	justify-content: center;
	min-height: 100%;

	.ant-modal-content {
		width: 100%;
		border-radius: 20px;
	}

	.ant-modal-close {
		top: 22px;
		right: 32px;
		position: absolute;

		span {
			width: 24px;
			height: 24px;
			display: block;
		}

		img,
		svg {
			user-select: none;
			display: block;
		}
	}

	.ant-modal-body {
		padding: 0;
		display: grid;
		grid-template-columns: minmax(0, 1fr) 408px;
	}
`

export default memo(Modal)
