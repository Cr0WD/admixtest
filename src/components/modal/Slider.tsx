import { FC, useEffect, useRef, useState } from 'react'
import { Carousel } from 'antd'
import styled from 'styled-components'
import { CarouselRef } from 'antd/es/carousel'
import { rgba } from 'polished'

import ArrowWhite from 'src/images/svg/slider/arrowWhite.svg'
import ArrowBlack from 'src/images/svg/slider/arrowBlack.svg'

const Slider: FC<{
	screenshots: string[] | undefined
}> = ({ screenshots }) => {
	if (!screenshots) screenshots = []

	const [activeIndex, setActiveIndex] = useState(0)

	/*
	 * To control sliders
	 * */
	const bigSlider = useRef<CarouselRef | null>()
	const smallSlider = useRef<CarouselRef | null>()

	useEffect(() => {
		bigSlider.current?.goTo(activeIndex)
		smallSlider.current?.goTo(activeIndex)
	}, [activeIndex])

	const beforeChange = (currentSlide: number, nextSlide: number) => setActiveIndex(nextSlide)

	return (
		<CarouselContainer>
			<StyledCarousel
				dots={false}
				arrows
				ref={slider => (bigSlider.current = slider)}
				beforeChange={beforeChange}
			>
				{screenshots.map((screenshot, index) => {
					return (
						<Slide key={index}>
							<div style={{ backgroundImage: `url(${screenshot})` }} />
						</Slide>
					)
				})}
			</StyledCarousel>
			<StyledSmallCarousel
				variableWidth
				ref={slider => (smallSlider.current = slider)}
				infinite
				beforeChange={beforeChange}
				dots={false}
				arrows
			>
				{screenshots.map((screenshot, index) => {
					return (
						<SmallSlide
							key={index}
							onClick={() => {
								smallSlider.current?.goTo(index)
							}}
						>
							<div style={{ backgroundImage: `url(${screenshot})` }} />
						</SmallSlide>
					)
				})}
			</StyledSmallCarousel>
		</CarouselContainer>
	)
}

const StyledSmallCarousel = styled(Carousel)`
	margin-left: -4px;

	.slick-track {
		padding: 24px 0;
	}

	.slick-arrow {
		margin: -12px;
		position: absolute;
		width: 24px;
		height: 24px;
		z-index: 3;

		transition: all 0.15s ease-in-out;

		border-radius: 8px;

		&:hover,
		&:focus {
			background-color: ${({ theme }) => rgba(theme.card.iconsBar.background, 0.1)};
		}

		&:active {
			transition: all 0s ease-in-out;
			background-color: ${({ theme }) => rgba(theme.card.iconsBar.background, 0.2)};
		}

		&,
		&:active,
		&:focus,
		&:hover {
			background-repeat: no-repeat;
			background-position: center;
			background-image: url('${ArrowBlack}');
		}

		&.slick- {
			&prev {
				left: -18px;

				transform: scale(-1, 1);
			}

			&next {
				right: -18px;
			}
		}
	}
`
const StyledCarousel = styled(Carousel)`
	border-radius: 8px;
	overflow: hidden;

	.slick-arrow {
		margin: 0;
		position: absolute;
		top: 0;
		width: 24px;
		height: 100%;
		z-index: 3;
		transition: all 0.15s ease-in-out;

		background-color: ${({ theme }) => rgba(theme.card.iconsBar.background, 0.6)};

		&:hover,
		&:focus {
			background-color: ${({ theme }) => rgba(theme.card.iconsBar.background, 0.8)};
		}

		&:active {
			transition: all 0s ease-in-out;
			background-color: ${({ theme }) => rgba(theme.card.iconsBar.background, 0.9)};
		}

		&,
		&:active,
		&:focus,
		&:hover {
			background-repeat: no-repeat;
			background-position: center;
			background-image: url('${ArrowWhite}');
		}

		&.slick- {
			&prev {
				left: 0;
			}

			&next {
				right: 0;
				transform: scale(-1, 1);
			}
		}
	}
`
const Slide = styled.div`
	> div {
		aspect-ratio: 934 / 556;
		background-size: cover;
	}
`

const SmallSlide = styled.div`
	padding: 0 4px;
	pointer-events: auto;
	transition: transform 0.15s ease-in-out;
	cursor: pointer;

	&:hover {
		transform: scale(1.05);
	}

	&:active {
		transform: scale(1.03);
	}

	> div {
		border-radius: 8px;
		background-size: cover;
		height: 100px;
		aspect-ratio: 172 / 100;
	}
`

const CarouselContainer = styled.div`
	margin-top: 40px;

	.slick-slider {
		position: relative;
	}
`

export default Slider
