import { memo } from 'react'
import nFormatter from 'src/utils/nFormatter'
import GenderSplitImage from 'src/images/svg/pictures/genderSplit.svg'
import AgeAudienceImage from 'src/images/svg/pictures/ageAudience.svg'
import TopCountriesImage from 'src/images/svg/pictures/mapStats.svg'
import MapImage from 'src/images/svg/pictures/map.svg'
import styled from 'styled-components'
import labels from 'src/constants/labels'
import { useRecoilValue } from 'recoil'
import { selectedItemState } from 'src/atoms/selectedItemState'
import { App } from 'src/interfaces/app'

const RightSide = () => {
	const item = useRecoilValue(selectedItemState) as App
	const { metrics, avails } = item ?? {}
	const { dau, mau } = metrics ?? {}

	/*
	 * Use labels from storage
	 * */
	const {
		availsTitle,
		avgCpmTitle,
		dailyActiveUsers,
		monthlyActiveUsers,
		noInfo,
		ageAudienceTitle,
		genderSplitTitle,
		appDemographics,
		topCountries,
		appLocations,
		appMetrics,
	} = labels.card

	return (
		<StyledRightSide>
			<div className="infoSection">
				<h4>{appMetrics}</h4>
				<div className="metrics">
					<dl>
						<dd>{nFormatter(avails ?? 0, 1, noInfo)}</dd>
						<dt>{availsTitle}</dt>
					</dl>
					<dl>
						<dd>
							{/* There is no info how to calc this info, so it will be like in designs */}
							{(0.051).toLocaleString('en-US', { maximumFractionDigits: 2 })}$
						</dd>
						<dt>{avgCpmTitle}</dt>
					</dl>
					<dl>
						<dd>{nFormatter(mau ?? 0, 1, noInfo)}</dd>
						<dt>{monthlyActiveUsers}</dt>
					</dl>
					<dl>
						<dd>{nFormatter(dau ?? 0, 1, noInfo)}</dd>
						<dt>{dailyActiveUsers}</dt>
					</dl>
				</div>
			</div>
			<div className="infoSection">
				<h4>{appDemographics}</h4>
				<div className="infoList">
					<dl>
						<dt>{genderSplitTitle}</dt>
						<dd>
							<img src={GenderSplitImage} alt={genderSplitTitle} />
						</dd>
					</dl>
					<dl>
						<dt>{ageAudienceTitle}</dt>
						<dd>
							<img
								src={AgeAudienceImage}
								alt={ageAudienceTitle}
								style={{
									width: '92%',
									position: 'relative',
									left: 11,
									top: -4,
									/*
									 * To fix mock graph position
									 * */
								}}
							/>
						</dd>
					</dl>
				</div>
			</div>

			<div className="infoSection">
				<h4>{appLocations}</h4>
				<div className="infoList">
					<dl>
						<dt>{topCountries}</dt>
						<dd>
							<img
								src={TopCountriesImage}
								alt={topCountries}
								style={{
									position: 'relative',
									top: -4,
									/*
									 * To fix mock graph position
									 * */
								}}
							/>
						</dd>
					</dl>
					<div className="mapContainer">
						<img src={MapImage} alt={appLocations} />
					</div>
				</div>
			</div>
		</StyledRightSide>
	)
}

const StyledRightSide = styled.div`
	padding: 42px 36px;

	h4 {
		padding-bottom: 16px;
		margin: 0;
		font-weight: normal;
		font-size: 20px;
		line-height: 140%;
		color: ${({ theme }) => theme.black600};
	}

	border-left: 1px solid ${({ theme }) => theme.gray};

	.infoSection {
		padding: 24px 0;

		& + .infoSection {
			border-top: 1px solid ${({ theme }) => theme.gray};
		}
	}

	.mapContainer {
		user-select: none;
		margin-top: 10px;
		position: relative;
		width: 104%;
		left: -10px;
		/*
		* To fix mock graph position
		* */
	}

	dt {
		font-weight: 600;
		font-size: 13px;
		line-height: 138%;
		color: ${({ theme }) => theme.grayText};
	}

	.infoList {
		display: grid;
		gap: 16px;
	}

	dl {
		padding: 0;
		display: grid;
		gap: 4px;
		margin: 0;
	}

	dd {
		padding: 0;
		margin: 0;
		font-size: 18px;
		line-height: 100%;
		font-weight: 800;
		color: ${({ theme }) => theme.secondary};

		img,
		svg {
			user-select: none;
			margin-top: 12px;
			display: block;
		}
	}

	.metrics {
		display: grid;
		grid-template-columns: auto auto;
		gap: 4px;
		row-gap: 16px;

		dl {
			&:nth-child(2n) {
				text-align: right;
			}
		}
	}
`

export default memo(RightSide)
