import { memo } from 'react'
import styled from 'styled-components'
import { useRecoilValue } from 'recoil'
import { selectedItemState } from 'src/atoms/selectedItemState'
import { App } from 'src/interfaces/app'
import { Button } from 'antd'
import labels from 'src/constants/labels'
import { rgba } from 'polished'
import Slider from './Slider'
import LoremIpsum from 'src/constants/loremIpsum'

const LeftSide = () => {
	const { addToMyList } = labels.cardDetailed
	const item = useRecoilValue(selectedItemState) as App

	const { googlePlayStoreInfo, tags, title, appStoreInfo } = item ?? {}
	const { screenshots, icon, studio, contentRating } = googlePlayStoreInfo ?? appStoreInfo ?? {}

	/*
	 * Use icon placeholder if icon not exists
	 * */
	const iconImage =
		icon ?? `https://dummyimage.com/64X64/ffffff/000.png&text=${title[0].toUpperCase()}`

	/*
	 * Get first 4 tags with capitalized first latter
	 * */
	const capitalizedTags = new Set(
		tags
			? tags
					.map(tag => {
						return tag.charAt(0).toUpperCase() + tag.slice(1)
					})
					.splice(0, 4)
			: []
	)

	const tagsList = [...capitalizedTags].join(', ')
	return (
		<StyledLeftSide>
			<div className="header">
				<div className="iconContainer">
					<div className="icon" style={{ backgroundImage: `url(${iconImage})` }} />
				</div>
				<div className="textContainer">
					<div className="description">
						<div className="titleContainer">
							<h2 title={title}>{title}</h2>
						</div>
						<div className="tagsContainer">
							{contentRating && <div className="contentRating">{contentRating}</div>}
							{tagsList && <div className="tags">{tagsList}</div>}
							{studio && tagsList && <div className="divider">•</div>}
							{studio && <div className="studio">{studio}</div>}
						</div>
					</div>
					<div className="actionContainer">
						<StyledButton>{addToMyList}</StyledButton>
					</div>
				</div>
			</div>
			{screenshots && screenshots.length && <Slider {...{ screenshots }} />}
			<Description>
				<LoremIpsum />
			</Description>
		</StyledLeftSide>
	)
}

const Description = styled.div`
	margin-top: 16px;
	font-weight: 600;
	font-size: 14px;
	line-height: 143%;
	color: ${({ theme }) => theme.grayText};
`
const StyledButton = styled(Button)`
	display: block;
	height: auto;
	background: ${({ theme }) => rgba(theme.primary, 1)} none;
	border-radius: 10px;
	border: 0;
	padding: 0 36px;

	span {
		font-weight: 600;
		line-height: 48px;
		font-size: 16px;
		display: block;
		color: ${({ theme }) => theme.white};
	}

	transition: background-color 0.15s ease-in-out;

	&:hover,
	&:active,
	&:focus {
		background-color: ${({ theme }) => rgba(theme.primary, 0.9)};
		color: ${({ theme }) => theme.white};
	}
`

const StyledLeftSide = styled.div`
	padding: 60px 38px 64px 64px;
	position: relative;

	.tagsContainer {
		display: flex;
		align-content: flex-start;
		align-items: flex-start;

		.divider {
			margin: 0 8px;
		}

		.tags,
		.studio,
		.divider {
			align-self: center;
			font-weight: 600;
			font-size: 13px;
			line-height: 138%;
			color: ${({ theme }) => theme.grayText};
		}
	}

	.contentRating {
		line-height: 22px;
		padding: 0 6px;
		border: 1px solid ${({ theme }) => rgba(theme.card.iconsBar.background, 0.5)};
		color: ${({ theme }) => rgba(theme.card.iconsBar.background, 0.5)};
		box-sizing: border-box;
		border-radius: 6px;
		margin-right: 16px;
	}

	.header {
		width: 100%;
		align-items: end;
		display: grid;
		gap: 24px;
		grid-template-columns: 64px 1fr;

		.textContainer {
			width: 100%;
			display: grid;
			gap: 24px;
			grid-template-columns: auto 1fr;

			.actionContainer {
				display: grid;
				justify-content: end;
				align-items: center;
			}

			.description {
				position: relative;
				display: grid;
				gap: 4px;

				.titleContainer {
					width: 100%;
					overflow: hidden;
					h2 {
						padding: 0;
						font-weight: 800;
						font-size: 32px;
						line-height: 125%;
						color: ${({ theme }) => theme.black};
						margin: 0;
						width: 100%;
						white-space: nowrap;
						overflow: hidden;
						text-overflow: ellipsis;
					}
				}
			}
		}
	}

	.iconContainer {
		user-select: none;

		.icon {
			background-size: cover;
			width: 64px;
			height: 64px;
			filter: drop-shadow(0px 4px 12px ${({ theme }) => rgba(theme.primary, 0.2)});
			border-radius: 8px;
		}
	}
`

export default memo(LeftSide)
