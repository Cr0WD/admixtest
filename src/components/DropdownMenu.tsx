import React, { memo } from 'react'
import { Button, Menu } from 'antd'
import Dropdown from 'antd/lib/dropdown'
import { Categories } from 'src/constants/categories'
import { useRecoilState } from 'recoil'
import { categoryState } from 'src/atoms/categoryState'
import { ReactComponent as DropDownIcon } from '../images/svg/dropdown.svg'
import labels from 'src/constants/labels'
import styled from 'styled-components'
import { rgba } from 'polished'

const DropdownMenu = () => {
	const [category, setCategory] = useRecoilState(categoryState)
	const categoryList = [...new Set([labels.defaultCategory, ...Categories.map(c => c.name)])]
	const menu = (
		<Menu>
			{categoryList.map((category, index) => (
				<Menu.Item
					key={index}
					onClick={() => {
						if (!category) return
						setCategory(category)
					}}
				>
					{category}
				</Menu.Item>
			))}
		</Menu>
	)
	return (
		<DropdownContainer>
			<Dropdown overlay={menu} trigger={['click']} overlayStyle={{ width: 200 }}>
				<StyledButton>
					<div className="textContainer">{category}</div>
					<div className="imageContainer">
						<DropDownIcon />
					</div>
				</StyledButton>
			</Dropdown>
		</DropdownContainer>
	)
}

const DropdownContainer = styled.div`
	display: block;
	padding-bottom: 11px;
	margin-bottom: 16px;
	border-bottom: 1px solid ${({ theme }) => rgba(theme.grayText, 0.1)};
`

const StyledButton = styled(Button)`
	/*
	* Disable outline
	* */
	&[ant-click-animating-without-extra-node='true']::after {
		display: none;
	}

	&,
	&:hover,
	&:active,
	&:focus {
		outline: none;
		background: none;
		box-shadow: none;
	}
	width: auto;
	padding: 0;
	border: 0;
	display: grid;
	gap: 8px;
	grid-template-columns: auto 18px;
	align-items: center;
	line-height: 24px;
	.textContainer {
		font-weight: bold;
		font-size: 18px;
		line-height: 133%;
		color: ${({ theme }) => theme.black};
	}
	svg,
	img {
		display: block;
		width: 18px;
		height: 18px;
	}
`

export default memo(DropdownMenu)
