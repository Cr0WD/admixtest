import { FC, useEffect, useState } from 'react'
import { fetchList } from 'src/utils/fetchList'
import { useRecoilState, useRecoilValue } from 'recoil'
import { listState } from 'src/atoms/listState'
import { usePrevious } from 'src/utils/hooks/usePrevious'
import styled from 'styled-components'
import Application from 'src/components/Application'
import settings from 'src/constants/settings'
import { searchState } from 'src/atoms/searchState'
import { Filter } from 'src/interfaces/request'
import { Button } from 'antd'
import labels from 'src/constants/labels'
import { rgba } from 'polished'
import { loadingState } from 'src/atoms/loadingState'
import { categoryState } from 'src/atoms/categoryState'
import { App } from 'src/interfaces/app'

const AppsList: FC<{
	defaultList?: App[]
}> = ({ defaultList }) => {
	const category = useRecoilValue(categoryState)
	const search = useRecoilValue(searchState)
	const [loading, setLoading] = useRecoilState(loadingState)
	const [appList, setAppList] = useRecoilState(listState)

	const [page, setPage] = useState(0)
	const [totalCount, setTotalCount] = useState(0)

	const { pageSize } = settings

	const prevPage = usePrevious(page)
	const prevSearch = usePrevious(search)
	const prevCategory = usePrevious(category)

	useEffect(() => {
		/*
		 * Reset page on query change
		 * */
		setPage(0)
	}, [search, setPage, category])

	useEffect(() => {
		if (defaultList) return

		/*
		 * Do nothing if query is same as previous
		 * */
		if (page === prevPage && search === prevSearch && category === prevCategory) return
		;(async () => {
			setLoading(true)

			const filters: Filter[] = []
			const options = {
				pageSize,
				pageIndex: page,
				filters,
			}

			/*
			 * Reset tag filtering if default category selected
			 * */
			if (category !== labels.defaultCategory)
				options['filters'].push({
					name: 'tags',
					value: category,
					operator: 'in',
				})

			if (search)
				options['filters'].push({
					name: 'title',
					value: search,
					operator: 'like',
				})

			const { items, totalCount } = await fetchList(options)
			setLoading(false)
			setTotalCount(totalCount)
			if (search !== prevSearch || category !== prevCategory) {
				/*
				 * Remove previous items from list
				 * */
				setAppList([...items])
				return
			}
			setAppList([...appList, ...items])
		})()
	}, [
		page,
		setAppList,
		appList,
		prevPage,
		prevSearch,
		search,
		pageSize,
		setLoading,
		category,
		prevCategory,
		defaultList,
	])

	const list = defaultList ?? appList
	return (
		<>
			<StyledList>
				{list.map((item, index) => (
					<Application key={index} item={item} data-test-card />
				))}
				{/* Skeletons 👇 */}
				{loading && [...new Array(pageSize)].map((_, index) => <Application key={index} />)}
			</StyledList>
			{!loading && totalCount === 0 && <div>{labels.noResults}</div>}
			{totalCount !== 0 && totalCount !== appList.length && (
				<StyledLoadMoreBtn
					onClick={() => {
						setPage(p => p + 1)
					}}
				>
					{labels.loadMore}
				</StyledLoadMoreBtn>
			)}
		</>
	)
}

const StyledLoadMoreBtn = styled(Button)`
	margin-top: 30px;
	background: ${({ theme }) => rgba(theme.primary, 0.1)} none;
	border-radius: 10px;
	border: 0;
	padding: 0 16px;

	span {
		line-height: 32px;
		font-weight: 600;
		font-size: 16px;
		display: block;
		color: ${({ theme }) => theme.primary};
	}

	transition: background-color 0.15s ease-in-out;

	&:hover,
	&:active,
	&:focus {
		background-color: ${({ theme }) => rgba(theme.primary, 0.3)};
		color: ${({ theme }) => theme.primary};
	}
`

const StyledList = styled.div`
	display: grid;
	grid-template-columns: 1fr 1fr 1fr 1fr;
	gap: 24px;
	row-gap: 32px;
`

export default AppsList
