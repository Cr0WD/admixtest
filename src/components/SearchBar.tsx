import Button from 'antd/lib/button'
import Input from 'antd/lib/input'
import { FC, useState } from 'react'
import styled from 'styled-components'
import { ReactComponent as SearchIcon } from '../images/svg/search.svg'
import { ReactComponent as GeoIcon } from '../images/svg/geo.svg'
import labels from 'src/constants/labels'
import { lighten } from 'polished'
import { useRecoilValue, useSetRecoilState } from 'recoil'
import { searchState } from 'src/atoms/searchState'
import { loadingState } from 'src/atoms/loadingState'

import { LoadingOutlined } from '@ant-design/icons'
import { Spin } from 'antd'

export const SearchBar: FC = () => {
	const loading = useRecoilValue(loadingState)
	const setSearch = useSetRecoilState(searchState)
	const [searchValue, setSearchValue] = useState('')

	const { search, geo, submit } = labels.searchBar
	return (
		<StyledSearchBar
			onSubmit={e => {
				e.preventDefault()
				setSearch(searchValue)
			}}
		>
			<StyledInput
				placeholder={search}
				prefix={<SearchIcon />}
				value={searchValue}
				onChange={e => {
					setSearchValue(e.target.value)
				}}
			/>
			{/* Geo input ignored */}
			<StyledInput placeholder={geo} prefix={<GeoIcon />} />
			<StyledButton htmlType={'submit'}>
				{!loading ? (
					submit
				) : (
					<Spin indicator={<LoadingOutlined style={{ fontSize: 24 }} spin />} />
				)}
			</StyledButton>
		</StyledSearchBar>
	)
}

const StyledInput = styled(Input)`
	border: 0;
	border-radius: 0;
	background-color: transparent;
	line-height: 43px;
	padding-top: 0;
	padding-bottom: 0;

	&:first-child {
		border-top-left-radius: ${({ theme }) => theme.searchBar.radius};
		border-bottom-left-radius: ${({ theme }) => theme.searchBar.radius};
	}

	& + & {
		border-left: 1px solid ${({ theme }) => theme.gray};

		&:hover {
			border-color: inherit;
		}
	}

	.ant-input-prefix {
		padding-left: 0;
		padding-right: 7px;
	}

	svg,
	img {
		display: block;
		height: 18px;
		width: 18px;
	}

	[fill] {
		transition: all 0.15s ease-in-out;
	}

	&:focus-within {
		box-shadow: none;
		outline: none;

		[fill] {
			fill: ${({ theme }) => theme.primary};
		}
	}
`
const StyledButton = styled(Button)`
	height: 100%;
	border: 0;
	background-color: ${({ theme }) => theme.primary};
	color: ${({ theme }) => theme.white};
	border-radius: 0;
	font-weight: 600;
	font-size: 16px;
	line-height: 150%;
	border-top-right-radius: ${({ theme }) => theme.searchBar.radius};
	border-bottom-right-radius: ${({ theme }) => theme.searchBar.radius};

	&:hover,
	&:focus,
	&:active {
		color: ${({ theme }) => theme.white};
		background-color: ${({ theme }) => lighten(0.1, theme.primary)};
	}

	span {
		display: block;
		color: ${({ theme }) => theme.white};
	}
`
const StyledSearchBar = styled.form`
	display: grid;
	grid-template-columns: 50% auto 59px;
	background: ${({ theme }) => theme.white};
	border: 1px solid ${({ theme }) => theme.gray};
	border-radius: ${({ theme }) => theme.searchBar.radius};
	margin-bottom: 37px;
`
