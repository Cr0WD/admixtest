/*
 * Source: https://gist.github.com/Ksmike/2ae3ffcc66d0196982ce18c0bb648e38
 * */

export const Categories = [
	{
		name: 'Action',
		describe: 'Teen',
	},
	{
		name: 'Adventure',
		describe: 'Everyone +10',
	},
	{
		name: 'Racing',
		describe: '+4',
	},
	{
		name: 'Simulation',
		describe: 'Teen',
	},
	{
		name: 'Biking',
		describe: '+4',
	},
	{
		name: 'Animals',
		describe: '+4',
	},
	{
		name: 'Food',
		describe: '+4',
	},
	{
		name: 'Gambling',
		describe: '+18',
	},
	{
		name: 'Super Heroes',
		describe: 'Teen',
	},
	{
		name: 'Battle Royale',
		describe: 'Teen',
	},
	{
		name: 'Entertainment',
		describe: 'Teen',
	},
	{
		name: 'Sports',
		describe: '+4',
	},
	{
		name: 'Fishing',
		describe: '+4',
	},
	{
		name: 'Driving',
		describe: '+4',
	},
	{
		name: 'RPG',
		describe: 'Everyone +10',
	},
	{
		name: 'Puzzle',
		describe: '+4',
	},
]
