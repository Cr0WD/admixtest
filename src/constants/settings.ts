const settings = {
	pageSize: 16,
	childRating: ['Teen', '+4', 'Everyone', '+10'],
}

export default settings
