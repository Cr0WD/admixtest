const labels = {
	noResults: 'No results',
	defaultCategory: 'Popular games',
	loadMore: 'Load more',
	searchBar: {
		search: 'Search game...',
		geo: 'Geo location',
		submit: 'Go',
	},
	card: {
		appMetrics: 'App metrics',
		genderSplitTitle: 'Gender split',
		ageAudienceTitle: 'Age audience',
		noInfo: 'Unknown',
		appLocations: 'App locations',
		appDemographics: 'App demographics',
		topCountries: 'Top 3 countries',
		monthlyActiveUsers: 'Monthly active users',
		dailyActiveUsers: 'Daily active users',
		avgCpmTitle: 'Avg CPM',
		availsTitle: 'Daily avails',
		childrenAlt: 'Available for children',
		medalAlt: 'High score',
	},
	cardDetailed: {
		addToMyList: 'Add to my list',
	},
}

export default labels
