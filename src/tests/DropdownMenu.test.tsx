import { shallow } from 'enzyme'
import { RecoilRoot } from 'recoil'
import DropdownMenu from 'src/components/DropdownMenu'
import theme from 'src/styles/theme'
import { ThemeProvider } from 'styled-components'

test('renders dropdown menu', () => {
	const render = () =>
		shallow(
			<ThemeProvider theme={theme}>
				<RecoilRoot>
					<DropdownMenu />
				</RecoilRoot>
			</ThemeProvider>
		)
	expect(render().html()).toMatchSnapshot()
	expect(render()).toMatchSnapshot()
})
