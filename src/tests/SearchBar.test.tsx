import { shallow } from 'enzyme'
import { SearchBar } from 'src/components/SearchBar'
import { RecoilRoot } from 'recoil'
import theme from 'src/styles/theme'
import { ThemeProvider } from 'styled-components'

test('renders searchbar', () => {
	const render = () =>
		shallow(
			<ThemeProvider theme={theme}>
				<RecoilRoot>
					<SearchBar />
				</RecoilRoot>
			</ThemeProvider>
		)
	expect(render().html()).toMatchSnapshot()
	expect(render()).toMatchSnapshot()
})
