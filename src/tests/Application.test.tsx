import { shallow } from 'enzyme'
import { RecoilRoot } from 'recoil'
import Application from 'src/components/Application'
import { ThemeProvider } from 'styled-components'
import theme from 'src/styles/theme'

test('renders app card', () => {
	const render = () =>
		shallow(
			<ThemeProvider theme={theme}>
				<RecoilRoot>
					<Application />
				</RecoilRoot>
			</ThemeProvider>
		)
	expect(render()).toMatchSnapshot()
	expect(render().html()).toMatchSnapshot()
})
