import { shallow } from 'enzyme'
import { RecoilRoot } from 'recoil'
import Modal from 'src/components/modal/Modal'
import { ThemeProvider } from 'styled-components'
import theme from 'src/styles/theme'

test('renders modal', () => {
	const render = () =>
		shallow(
			<ThemeProvider theme={theme}>
				<RecoilRoot>
					<Modal />
				</RecoilRoot>
			</ThemeProvider>
		)
	expect(render().html()).toMatchSnapshot()
	expect(render()).toMatchSnapshot()
})
