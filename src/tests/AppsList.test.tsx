import { RecoilRoot } from 'recoil'
import { MockList } from 'src/tests/mockList'
import { mount } from 'enzyme'
import AppsList from 'src/components/AppsList'
import theme from 'src/styles/theme'
import { ThemeProvider } from 'styled-components'

describe('Applist', () => {
	const render = () =>
		mount(
			<RecoilRoot>
				<ThemeProvider theme={theme}>
					<AppsList defaultList={MockList} />
				</ThemeProvider>
			</RecoilRoot>
		)

	it('renders list from mock', () => {
		expect(render().html()).toMatchSnapshot()
		expect(render()).toMatchSnapshot()
	})

	it('renders all items', () => {
		expect(render().find('[data-test-card]').length).toBe(MockList.length)
	})
})
