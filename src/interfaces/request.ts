import { App } from 'src/interfaces/app'

export interface Body {
	pageIndex?: number
	pageSize?: number
	operator?: 'and' | 'or'
	filters?: Filter[]
	sorts?: Sort[]
}

export interface Filter {
	name: keyof App
	value: App[keyof App] | Array<App[keyof App]>
	operator: 'gt' | 'lt' | 'eq' | 'like' | 'in'
}

export interface Sort {
	field: keyof App
	desc: boolean
}
