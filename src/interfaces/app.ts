type isoString = string

export interface App {
	title: string
	tags: string[]
	avails?: number
	score?: number
	platform?: 'mobile' | 'vr' | 'ar'
	geos?: string[]
	storeCategories?: string
	updatedAt?: Date | isoString
	createdAt?: Date | isoString

	metrics: AppMetrics

	googlePlayStoreInfo?: StoreInfo
	appStoreInfo?: StoreInfo
}

export interface StoreInfo {
	contentRating?: string
	genre?: string
	icon?: string
	score?: number
	screenshots?: string[]
	studio?: string
	title?: string
	url?: string
}

export interface AppMetrics {
	avgTimePerSession?: number
	dau?: number | null
	mau?: number | null
	sessions?: number
}
